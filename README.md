cataclysm-game
==============

Development build of Cataclysm, a text adventure game built in Python.

Installing and Running
------------

### Linux / Mac OSX
Extract the game from the compressed file.

`chmod +x /path/to/cataclysm-game/cataclysm.py`

`cd /path/to/cataclysm-game/`

`./cataclysm.py`

### Windows
[Click Me](http://docs.python.org/2/faq/windows.html)
