#!/usr/bin/env python
#Authored by Angus Gardner and Raymond Friedrick (C)2013

    #This program is free software: you can redistribute it and/or modify
    #it under the terms of the GNU General Public License as published by
    #the Free Software Foundation, either version 3 of the License, or
    #(at your option) any later version.

    #This program is distributed in the hope that it will be useful,
    #but WITHOUT ANY WARRANTY; without even the implied warranty of
    #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #GNU General Public License for more details.

    #You should have received a copy of the GNU General Public License
    #along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
import sys
import os
import time
from assets.events import Event

#definitions
def print_slowly_loading(text):
    		for c in text:
        		print c,
        		sys.stdout.flush()
        		time.sleep(0.5)

def print_slowly(text):
			for c in text:
				print c,
				sys.stdout.flush()
				time.sleep(1)

def die(args):
	    global dead
	    print
	    if args:
	        print "You are dead. Your story has ended, and what else is left to uncover has been lost forever."
	    dead = True
	    exit(1)
	    
#indexing/locations
def in_chap1_cell1(): #cell you start in
	blk=0
def in_chap1_hall_b1(): #immediately exiting cell
	blk=1
def in_chap1_hall_b2(): #north end of corridor
	blk=2
def in_chap1_hall_b3(): #moving east after north end of corridor
	blk=3
def in_chap1_hall_b4(): #south end of corridor
	blk=4
def in_chap1_warden(): #wardens office
	blk=5
def in_chap1_mess(): #mess hall
	blk=6
def in_chap1_prison_storage(): #prison storage
	blk=7

#indexing/inventory
items = []

os.system('cls' if os.name=='nt' else 'clear')
##start introduction
print "Developer build 0.1.4"
print "Copyright (c) 2013, 2014, Angus Gardner and Raymond Friedrick. All rights reserved."
time.sleep(0.5)
print " _    _      _                            _          _   _                               _     _          __ "
print "| |  | |    | |                          | |        | | | |                             | |   | |        / _|"
print "| |  | | ___| | ___ ___  _ __ ___   ___  | |_ ___   | |_| |__   ___  __      _____  _ __| | __| |   ___ | |_ "
print "| |/\| |/ _ \ |/ __/ _ \| '_ ` _ \ / _ \ | __/ _ \  | __| '_ \ / _ \ \ \ /\ / / _ \| '__| |/ _` |  / _ \|  _|"
print "\  /\  /  __/ | (_| (_) | | | | | |  __/ | || (_) | | |_| | | |  __/  \ V  V / (_) | |  | | (_| | | (_) | |  "
print " \/  \/ \___|_|\___\___/|_| |_| |_|\___|  \__\___/   \__|_| |_|\___|   \_/\_/ \___/|_|  |_|\__,_|  \___/|_|  "
time.sleep(1)
print ""
print "                           _____   ___ _____ ___  _____  _   __   __________  ___"
print "                          /  __ \ / _ \_   _/ _ \/  __ \| |  \ \ / /  ___|  \/  |"
print "                          | /  \// /_\ \| |/ /_\ \ /  \/| |   \ V /\ `--.| .  . |"
print "                          | |    |  _  || ||  _  | |    | |    \ /  `--. \ |\/| |"
print "                          | \__/\| | | || || | | | \__/\| |____| | /\__/ / |  | |"
print "                           \____/\_| |_/\_/\_| |_/\____/\_____/\_/ \____/\_|  |_/"
time.sleep(3)
print ""
print"                                       Written by Raymond F and Angus G"
time.sleep(1.75)
print""
print"In CATACLYSM you are an adventurer, on the path to discover your past and help those in need along the way."
time.sleep(2)
print""
print"It all started in the second era, on the tenth cycle. War had torn the seven kingdoms apart and many cities laid in ruin to the ruthless warriors of Lord Raganherhd."
time.sleep(3)
print""
print"Now begins your story."
##end introduction

##start instructions
print ""
choice_instructions = str(raw_input("""A voice echos, "Do you know what you're doing?" (Y/N) """))
if choice_instructions in ('N', 'n', 'No', 'no'):
	pass
	print ""
	print "If you choose to explore further, then use the cardinal directions."
	print "If you wish to take a closer look at anything use look and then the object you wish to look at."
	print "If you believe you can use an item or object, attempt to use it. You can also take items for future use if you think you can carry them."
	print "Some items you come across on your endevours can be combined to solve puzzles."
	print "(The main commands are: move n/s/e/w, look (object), take (item), use (item) [optional] on (object), combine (item 1) and (item 2)"
	print "To combine an item you have to be at a workbench.\n"
	str(raw_input("Press enter to continue"))
if choice_instructions != ('N', 'n', 'No', 'no'):
	print "Loading ",
	print_slowly_loading('... ')
##end instructions

##start first chapter
game=0
on=315
game=on
in_chap1_cell1 = True
loose_stone = True
chap1_cell1_gate = True
os.system('cls' if os.name=='nt' else 'clear')
print """
Your hands press against the rough, cold stone material. The lids
of your eyes eerily pry open, and you find yourself within a jail cell.
Several beams of light shine through cracks in the stone and you stand.
Below you rests a straw bed, a single faded white sheet lays crumpled at the end.
A satchel hangs from your neck, with the single strap holding it to your side.
It could be used to store things.
To your left is a table with a small clay bowl resting in the middle. 
Nothing else is notable, other than the barred entrance in front of you."""
print ""
print "You stand in the center of the cell."

while game == on:

	while in_chap1_cell1 == True:
		pass
		chap1_cell_op = str(raw_input("\n"))

		if chap1_cell_op == 'look n':
			pass
			print "You stare at the barren stone wall to the north. A single stone brick seems loose."
		elif chap1_cell_op == 'look s':
			pass
			print "You stare at the barren stone wall to the south. A ripped red flag hangs from the wall."
			print "The fading shape of a lion is painted on it."
		elif chap1_cell_op == 'look e':
			pass
			print "You look ahead, the arch leads to a dark hallway."
		elif chap1_cell_op == 'look w':
			pass
			print "Turning to the wall behind you, the bright beams of light shine in your eyes,"
			print "giving you temporary blindness for a few seconds. You turn away, adjusting to the dark again."
		elif chap1_cell_op == 'look up':
			pass
			print "You look up to see nothing but the crumbling stone ceiling."
		elif chap1_cell_op == 'look down':
			pass
			print "You stand, aimlessly looking at the floor."
		elif chap1_cell_op == 'look bed':
			pass
			print "Below you is the bed you woke from."
			print "On it is a bundled white sheet, and under is straw strewn around."
		elif chap1_cell_op in ('look loose stone', 'look stone', 'look stone brick'):
			if loose_stone == False:
				if "cell-key" in items:
					pass
					print "You look at the hole where the stone once was."
					continue
				pass
				print "You look at the hole where the stone once was. The shape of a key is seen, with a faint golden"
				print "glow reflecting off of it."
				continue
			pass
			print "You look at the stone hanging slightly out from the wall, its sides seem cracked in slightly from where it should be tightened."
		elif chap1_cell_op == 'look bowl':
			if "bowl" in items:
				pass
				print "You reach into your satchel to take a look at the bowl."
				print "It is made of clay, aged and covered in dust. The side displays engravings of slaves dragging large stones."
				continue
			pass
			print "The bowl rests on the table. It is made of clay, aged and covered in dust. The side displays"
			print "engravings of slaves dragging large stones."

		elif chap1_cell_op == 'move n':
			pass
			print "You can't go that way."

		elif chap1_cell_op == 'move s':
			pass
			print "You can't go that way."

		elif chap1_cell_op == 'move e':
			if chap1_cell1_gate == False:
				pass
				in_chap1_cell1 = False
				in_chap1_hall_b1 = True
				print "You walk into the hallway.\n"
				print "You look to the north, down the hall."
				print "Cells line the walls, and at the end it turns"
				print "east to darkness.\n"
				print "You look to the south, up the hall."
				print "Cells line the walls, abruptly ending at a door."
				print "The door reads in faded text, 'WARDEN'.\n"
				print "You stand in the center of the hall."
				continue
			pass
			print "The gate ahead is closed. A keyhole sits on the right side, empty."

		elif chap1_cell_op == 'move w':
			pass
			print "You can't go that way."

		elif chap1_cell_op == 'take bowl':
			if "clay-bowl" in items:
				pass
				print "The table has nothing on it, and it seems like you already have the bowl in your satchel."
				continue
			pass
			items.append ("clay-bowl")
			print "You reach out to the table, taking the small clay bowl in your hands."
			print "Not knowing what to do with it, you place it in the satchel hanging from your neck."
		elif chap1_cell_op == 'take key':
			if loose_stone == False:
				if 'cell-key' in items:
					pass
					print "There is no key in the cell, you seem to already have it in your satchel."
					continue
				pass
				items.append ("cell-key")
				print "You reach out into the small hole from which you pulled the stone, taking the glimmering golden key in your hand."
				continue
			elif loose_stone == True:
				pass
				print "Try again."
				continue

		elif chap1_cell_op == 'use key on gate':
			if 'cell-key' in items:
				pass
				print "You slide the small golden key into the keyhole, twisting it right. A faint click emits from the gate."
				items.remove ("cell-key")
				chap1_cell1_gate = False
				continue
			pass
			print "You don't have a key."
		elif chap1_cell_op in ('use loose stone', 'use stone', 'use stone brick'):
			if loose_stone == False:
				pass
				print "The stone has already been removed from the wall."
				continue
			pass
			loose_stone = False
			print "You reach out, sliding your fingertips to the edges of the stone. Pulling back, it falls onto the"
			print "floor below, crumbling to pieces."

		elif chap1_cell_op == 'inventory':
			pass
			print items 

		else:
			pass
			print "Try again."

	while in_chap1_hall_b1 == True:
		pass
		chap1_hall1_b1_op = str(raw_input("\n"))

		if chap1_hall1_b1_op == 'look n':
			pass
			print "You look to the north, down the hall."
			print "Cells line the walls, and at the end it turns east to darkness."
		elif chap1_hall1_b1_op == 'look s':
			pass
			print "You look to the south, up the hall."
			print "Cells line the walls, abruptly ending at a door."
			print "The door reads in faded text, 'WARDEN'."
		elif chap1_hall1_b1_op == 'look e':
			pass
			print "Looking east, you see a dark cell, barred."
			print "It seems empty."
		elif chap1_hall1_b1_op == 'look w':
			pass
			print "You look back to the cell you started in."
			print "Several beams of light shine in your eyes."

		elif chap1_hall1_b1_op == 'move n':
			pass
			in_chap1_hall_b1 = False
			in_chap1_hall_b2 = True
			print "You move north, reaching the end of the corridor;"
			print "to the corner turning east."
			print "You can see down the hall is a door that reads 'MESS HALL',"
			print "and an orange light shining from inside."

		elif chap1_hall1_b1_op == 'move s':
			pass
			in_chap1_hall_b1 = False
			in_chap1_hall_b4 = True
			print "Passing the empty cells, you meet the door."
			print "You stop in front, considering opening it."

		elif chap1_hall1_b1_op == 'move e':
			pass
			print "You can't go that way."

		elif chap1_hall1_b1_op == 'move w':
			pass
			in_chap1_hall_b1 = False
			in_chap1_cell1 = True
			print "You walk back into the cell you started in.\n"
			print "You stand in the center of the cell."

		elif chap1_hall1_b1_op == 'take item':
			if 'item' in items:
				pass
				print "You have that item."
				continue
			items.append('item')
			print "You take the item."

		elif chap1_hall1_b1_op == 'use object or item':
			if 'item' in items:
				pass
				print "You use item."
			print "You can't do that."

		elif chap1_hall1_b1_op == 'inventory':
			pass
			print items

		else:
			pass
			print "Try again."

	while in_chap1_hall_b4 == True:

		chap1_hall_b4_op = str(raw_input("\n"))

		if chap1_hall_b4_op == 'use door':
			pass
			print "You reach out, gripping the door handle and starting to turn it."
			print "The handle stops, the door seems to be locked."
		elif chap1_hall_b4_op == 'move n':
			pass
			in_chap1_hall_b4 = False
			in_chap1_hall_b1 = True
			print "You walk back to where you exited the cell."
		elif chap1_hall_b4_op == 'use key on door':
			pass
			if "warden-key" in items:
				pass
				print "You take from your satchel the key, sliding it into the door handle's keyhole.\n"
				items.remove ("warden-key")
				in_chap1_hall_b4 = False
				in_chap1_warden = True
				print "You walk into the warden's office."
			else:
				pass
				print "You don't have a key. Look for it."
		elif chap1_hall_b4_op == 'inventory':
			pass
			print items
		else:
			pass
			print "Try again."

	while in_chap1_hall_b2 == True:

		chap1_hall1_b2_op = str(raw_input("\n"))

		if chap1_hall1_b2_op == 'move e':
			pass
			in_chap1_hall_b2 = False
			in_chap1_hall_b3 = True
			print "Walking down the corridor, you find yourself in the middle, looking at the door."
		elif chap1_hall1_b2_op == 'move s':
			pass
			in_chap1_hall_b2 = False
			in_chap1_hall_b1 = True
			print "You walk back to where you exited the cell."
		elif chap1_hall1_b2_op == 'inventory':
			pass
			print items
		else:
			pass
			print "Try again."

	while in_chap1_hall_b3 == True:

		chap1_hall1_b3_op = str(raw_input("\n"))

		if chap1_hall1_b3_op == 'move e':
			pass
			in_chap1_hall_b3 = False
			in_chap1_mess = True
			print "You push past the thick wooden door into the mess hall."
		elif chap1_hall1_b3_op == 'inventory':
			pass
			print items

	while in_chap1_mess == True:

		chap1_mess_op = str(raw_input("\n"))

		if chap1_mess_op == 'move w':
			pass
			in_chap1_mess = False
			in_chap1_hall_b3 = True
			print "You push out of the mess hall, moving back into the corridor."
		elif chap1_mess_op == 'inventory':
			pass
			print items
